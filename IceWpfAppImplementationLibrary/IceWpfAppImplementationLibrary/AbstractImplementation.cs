﻿using System.Collections.Generic;
using System.Threading;
using System.Windows.Threading;
using CAALHP.Contracts;
using System.Windows;

namespace IceWpfAppImplementationLibrary
{
    public abstract class AbstractImplementation : IAppCAALHPContract
    {
        #region Members
        private Thread _thread;
        protected volatile Application _app;
        private IAppHostCAALHPContract _host;
        private int _processId;
        #endregion

        #region Abstract Methods
        public abstract string GetName();
        public abstract void Show();
        public abstract void Notify(KeyValuePair<string, string> notification);
        #endregion

        public bool IsAlive()
        {
            return true;
        }

        /// <summary>
        /// called by host when application is being forced to shutdown. Overload if application specific things has to be done on shutdown.
        /// </summary>
        public void ShutDown()
        {
            _app.Dispatcher.BeginInvokeShutdown(DispatcherPriority.Normal);
        }

        /// <summary>
        /// called by the host, to initialize the plugin.
        /// </summary>
        /// <param name="hostObj"></param>
        /// <param name="processId"></param>
        public void Initialize(IAppHostCAALHPContract hostObj, int processId)
        {
            _host = hostObj;
            _processId = processId;
        }


    }
}
